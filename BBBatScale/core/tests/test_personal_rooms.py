from core.models import GeneralParameter, HomeRoom, PersonalRoom, RoomConfiguration, SchedulingStrategy, User
from django.conf import settings
from django.contrib.auth.models import Group
from django.contrib.sites.models import Site
from django.test import TestCase


class CreateHomeRoomTestCase(TestCase):
    def setUp(self):
        self.scheduling_strategy_fbi = SchedulingStrategy.objects.create(
            name="Fachbereich Informatik",
        )

        self.room_config_1 = RoomConfiguration.objects.create()

        self.tenant = Site.objects.create(domain="testserver", name="testserver")
        self.general_parameters: GeneralParameter = GeneralParameter.load(self.tenant)
        self.general_parameters.home_room_enabled = True
        self.general_parameters.home_room_teachers_only = True
        self.general_parameters.home_room_scheduling_strategy = self.scheduling_strategy_fbi
        self.general_parameters.home_room_room_configuration = self.room_config_1
        self.general_parameters.save()

        self.moderator_group = Group.objects.create(name=settings.MODERATORS_GROUP)
        self.moderator_group.save()

    def tearDown(self):
        pass

    def test_home_room_create_with_email_name(self):
        self.teacher_user: User = User.objects.create_user(
            username="teacher-1",
            password="secret",
            email="teacher-1@example.org",
            first_name="Jane J.",
            last_name="Doe",
            tenant=self.tenant,
        )
        self.teacher_user.groups.add(self.moderator_group)
        self.teacher_user.save()

        self.assertTrue("home-teacher-1@example.org" == self.teacher_user.homeroom.name)

    def test_home_room_create_with_email_not_set(self):
        self.teacher_user: User = User.objects.create_user(
            username="teacher-1", password="secret", first_name="Jane J.", last_name="Doe", tenant=self.tenant
        )
        self.teacher_user.groups.add(self.moderator_group)
        self.teacher_user.save()

        self.assertTrue("home-Doe-" in self.teacher_user.homeroom.name)

    def test_home_room_create_two_users_same_email(self):
        self.teacher_user_1: User = User.objects.create_user(
            username="teacher-1",
            password="secret",
            email="teacher-1@example.org",
            first_name="Jane J.",
            last_name="Doe",
            tenant=self.tenant,
        )
        self.teacher_user_1.groups.add(self.moderator_group)
        self.teacher_user_1.save()
        self.assertTrue("home-teacher-1@example.org" == self.teacher_user_1.homeroom.name)

        self.teacher_user_2: User = User.objects.create_user(
            username="teacher-2",
            password="secret",
            email="teacher-1@example.org",
            first_name="Jane J.",
            last_name="Doe",
            tenant=self.tenant,
        )
        self.teacher_user_2.groups.add(self.moderator_group)
        self.teacher_user_2.save()
        self.assertTrue("home-Doe-" in self.teacher_user_2.homeroom.name)

    def test_home_room_created_for_teacher_only(self):
        self.teacher_user: User = User.objects.create_user(
            username="teacher-1",
            password="secret",
            email="teacher-1@example.org",
            first_name="Jane J.",
            last_name="Doe",
            tenant=self.tenant,
        )
        self.teacher_user.groups.add(self.moderator_group)
        self.teacher_user.save()

        self.student_user: User = User.objects.create_user(
            username="student-1",
            password="secret",
            email="student-1@example.org",
            first_name="John S.",
            last_name="Doe",
            tenant=self.tenant,
        )

        self.assertEqual(HomeRoom.objects.count(), 0)
        self.assertIsNotNone(self.teacher_user.homeroom)
        self.assertIsNone(self.student_user.homeroom)
        self.assertEqual(HomeRoom.objects.count(), 1)

    def test_home_room_created_for_all_users(self):
        self.general_parameters.home_room_teachers_only = False
        self.general_parameters.save()

        self.teacher_user: User = User.objects.create_user(
            username="teacher-1",
            password="secret",
            email="teacher-1@example.org",
            first_name="Jane J.",
            last_name="Doe",
            tenant=self.tenant,
        )
        self.teacher_user.groups.add(self.moderator_group)
        self.teacher_user.save()

        self.student_user: User = User.objects.create_user(
            username="student-1",
            password="secret",
            email="student-1@example.org",
            first_name="John S.",
            last_name="Doe",
            tenant=self.tenant,
        )

        self.assertEqual(HomeRoom.objects.count(), 0)
        self.assertIsNotNone(self.teacher_user.homeroom)
        self.assertIsNotNone(self.student_user.homeroom)
        self.assertEqual(HomeRoom.objects.count(), 2)

    def test_home_room_delete_on_user_delete(self):
        self.teacher_user: User = User.objects.create_user(
            username="teacher-1",
            password="secret",
            email="teacher-1@example.org",
            first_name="Jane J.",
            last_name="Doe",
            tenant=self.tenant,
        )
        self.teacher_user.groups.add(self.moderator_group)
        self.teacher_user.save()

        self.student_user: User = User.objects.create_user(
            username="student-1",
            password="secret",
            email="student-1@example.org",
            first_name="John S.",
            last_name="Doe",
            tenant=self.tenant,
        )

        self.assertIsNotNone(self.teacher_user.homeroom)
        self.assertIsNone(self.student_user.homeroom)

        self.teacher_user.delete()
        self.student_user.delete()

        self.assertEqual(HomeRoom.objects.count(), 0)

    def test_home_room_can_not_be_accessed(self):
        self.teacher_user: User = User.objects.create_user(
            username="teacher-1",
            password="secret",
            email="teacher-1@example.org",
            first_name="Jane J.",
            last_name="Doe",
            tenant=self.tenant,
        )
        self.teacher_user.groups.add(self.moderator_group)
        self.teacher_user.save()

        self.student_user: User = User.objects.create_user(
            username="student-1",
            password="secret",
            email="student-1@example.org",
            first_name="John S.",
            last_name="Doe",
            tenant=self.tenant,
        )

        self.assertIsNotNone(self.teacher_user.homeroom)
        self.assertIsNone(self.student_user.homeroom)
        self.assertEqual(HomeRoom.objects.count(), 1)

        self.general_parameters.home_room_enabled = False
        self.general_parameters.save()

        self.assertIsNone(self.teacher_user.homeroom)
        self.assertIsNone(self.student_user.homeroom)
        self.assertEqual(HomeRoom.objects.count(), 1)


class UpdateHomeRoomGeneralParametersTestCase(TestCase):
    def setUp(self):
        self.scheduling_strategy_fbi = SchedulingStrategy.objects.create(
            name="Fachbereich Informatik",
        )

        self.room_config_1 = RoomConfiguration.objects.create(name="config-1")

        self.tenant = Site.objects.create(domain="testserver", name="testserver")
        self.general_parameters: GeneralParameter = GeneralParameter.load(self.tenant)
        self.general_parameters.home_room_enabled = True
        self.general_parameters.home_room_teachers_only = True
        self.general_parameters.home_room_scheduling_strategy = self.scheduling_strategy_fbi
        self.general_parameters.home_room_room_configuration = self.room_config_1
        self.general_parameters.save()

        self.teacher_user: User = User.objects.create_user(
            username="teacher-1",
            password="secret",
            email="teacher-1@nothing.com",
            first_name="Jane J.",
            last_name="Doe",
            tenant=self.tenant,
        )

        self.moderator_group = Group.objects.create(name=settings.MODERATORS_GROUP)
        self.moderator_group.save()

        self.teacher_user.groups.add(self.moderator_group)
        self.teacher_user.save()

        self.student_user: User = User.objects.create_user(
            username="student-1",
            password="secret",
            email="student-1@nothing.com",
            first_name="John S.",
            last_name="Doe",
            tenant=self.tenant,
        )

    def tearDown(self):
        pass

    def clean_home_rooms(self):
        self.general_parameters.home_room_teachers_only = True
        self.general_parameters.save()

        HomeRoom.objects.all().delete()
        self.assertIsNotNone(self.teacher_user.homeroom)
        self.assertIsNone(self.student_user.homeroom)
        self.assertEqual(HomeRoom.objects.count(), 1)

    def test_home_room_enabled(self):
        HomeRoom.objects.all().delete()
        self.assertEqual(HomeRoom.objects.count(), 0)

        self.general_parameters.home_room_enabled = False
        self.general_parameters.save()

        self.assertEqual(HomeRoom.objects.count(), 0)
        self.assertIsNone(self.teacher_user.homeroom)
        self.assertEqual(HomeRoom.objects.count(), 0)

        self.general_parameters.home_room_enabled = True
        self.general_parameters.save()

        self.assertEqual(HomeRoom.objects.count(), 0)
        self.assertIsNotNone(self.teacher_user.homeroom)
        self.assertEqual(HomeRoom.objects.count(), 1)

        self.general_parameters.home_room_enabled = False
        self.general_parameters.save()

        self.assertEqual(HomeRoom.objects.count(), 1)
        self.assertIsNone(self.teacher_user.homeroom)
        self.assertEqual(HomeRoom.objects.count(), 1)

        self.general_parameters.home_room_enabled = True
        self.general_parameters.save()

        self.assertEqual(HomeRoom.objects.count(), 1)
        self.assertIsNotNone(self.teacher_user.homeroom)
        self.assertEqual(HomeRoom.objects.count(), 1)

    def test_home_room_teachers_only(self):
        self.clean_home_rooms()

        self.general_parameters.home_room_teachers_only = False
        self.general_parameters.save()

        # rooms exist for both student and teacher
        self.assertIsNotNone(self.student_user.homeroom)
        self.assertEqual(HomeRoom.objects.count(), 2)

        self.general_parameters.home_room_teachers_only = True
        self.general_parameters.save()

        # rooms exist for both but only teacher can access it
        self.assertIsNotNone(self.teacher_user.homeroom)
        self.assertIsNone(self.student_user.homeroom)
        self.assertEqual(HomeRoom.objects.count(), 2)

    def test_change_home_room_scheduling_strategy(self):
        self.clean_home_rooms()

        self.assertEqual(self.teacher_user.homeroom.scheduling_strategy.pk, self.scheduling_strategy_fbi.pk)

        scheduling_strategy_2 = SchedulingStrategy.objects.create(name="New Tenant")

        self.general_parameters.home_room_teachers_only = False
        self.general_parameters.home_room_scheduling_strategy = scheduling_strategy_2
        self.general_parameters.save()

        # preserve already set scheduling_strategys
        self.assertEqual(self.teacher_user.homeroom.scheduling_strategy.pk, self.scheduling_strategy_fbi.pk)
        self.assertEqual(self.student_user.homeroom.scheduling_strategy.pk, scheduling_strategy_2.pk)

    def test_unset_home_room_scheduling_strategy(self):
        self.clean_home_rooms()

        self.assertEqual(self.teacher_user.homeroom.scheduling_strategy.pk, self.scheduling_strategy_fbi.pk)

        self.general_parameters.home_room_teachers_only = False
        self.general_parameters.home_room_scheduling_strategy = None
        self.general_parameters.save()

        # the student cannot create new homeroom although home_room_teachers_only is false
        self.assertEqual(self.teacher_user.homeroom.scheduling_strategy.pk, self.scheduling_strategy_fbi.pk)
        self.assertIsNone(self.student_user.homeroom)

    def test_change_home_room_room_configuration(self):
        self.clean_home_rooms()

        self.assertEqual(self.teacher_user.homeroom.config.pk, self.room_config_1.pk)

        room_config_2 = RoomConfiguration.objects.create(name="config-2")

        self.general_parameters.home_room_teachers_only = False
        self.general_parameters.home_room_room_configuration = room_config_2
        self.general_parameters.save()

        self.assertEqual(self.teacher_user.homeroom.config.pk, self.room_config_1.pk)
        self.assertEqual(self.student_user.homeroom.config.pk, room_config_2.pk)

    def test_unset_home_room_room_configuration(self):
        self.clean_home_rooms()

        self.assertEqual(self.teacher_user.homeroom.config.pk, self.room_config_1.pk)

        self.general_parameters.home_room_teachers_only = False
        self.general_parameters.home_room_room_configuration = None
        self.general_parameters.save()

        # the student cannot create new homeroom although home_room_teachers_only is false
        self.assertEqual(self.teacher_user.homeroom.scheduling_strategy.pk, self.scheduling_strategy_fbi.pk)
        self.assertIsNone(self.student_user.homeroom)


class CreatePersonalRoomsTestCase(TestCase):
    def setUp(self):
        self.scheduling_strategy = SchedulingStrategy.objects.create(name="FBI", token_registration="xxxxasdaxxcfaa")

        self.tenant = Site.objects.create(domain="testserver", name="testserver")
        self.gp = GeneralParameter.load(self.tenant)
        self.gp.personal_rooms_teacher_max_number = 8
        self.gp.personal_rooms_non_teacher_max_number = 0
        self.gp.home_room_scheduling_strategy = self.scheduling_strategy
        self.gp.save()

        self.moderator_group = Group.objects.create(name=settings.MODERATORS_GROUP)

        self.teacher_user_1: User = User.objects.create_user(
            username="teacher-1",
            password="secret",
            email="mailt1@example.org",
            first_name="Jane J.",
            last_name="Doe",
            tenant=self.tenant,
        )
        self.teacher_user_1.groups.add(self.moderator_group)
        self.teacher_user_1.save()

        self.teacher_user_2: User = User.objects.create_user(
            username="teacher-2",
            password="secret",
            email="mailt2@example.org",
            first_name="Jane J.",
            last_name="Doe",
            personal_rooms_max_number=3,
            tenant=self.tenant,
        )
        self.teacher_user_2.groups.add(self.moderator_group)
        self.teacher_user_2.save()

        self.student_user_1: User = User.objects.create_user(
            username="student-1",
            password="secret",
            email="mails1@example.org",
            first_name="John S.",
            last_name="Doe",
            tenant=self.tenant,
        )

        self.student_user_2: User = User.objects.create_user(
            username="student-2",
            password="secret",
            email="mails2@example.org",
            first_name="John S.",
            last_name="Doe",
            personal_rooms_max_number=5,
            tenant=self.tenant,
        )

        self.test_room_config = RoomConfiguration.objects.create()

    def tearDown(self):
        pass

    def test_personal_room_rooms_max_number(self):
        self.assertEqual(self.teacher_user_1.get_max_number_of_personal_rooms(), 8)
        self.assertEqual(self.teacher_user_2.get_max_number_of_personal_rooms(), 3)
        self.assertEqual(self.student_user_1.get_max_number_of_personal_rooms(), 0)
        self.assertEqual(self.student_user_2.get_max_number_of_personal_rooms(), 5)

    def test_owner_shall_not_be_co_owner(self):
        pr_test = PersonalRoom(
            name="testroom",
            owner=self.teacher_user_1,
            scheduling_strategy=self.gp.home_room_scheduling_strategy,
            is_public=False,
            config=self.test_room_config,
        )
        pr_test.save()

        pr_test.co_owners.add(pr_test.owner)
        pr_test.save()

        self.assertEqual(len(pr_test.co_owners.all()), 0)

    def test_personal_room_create_room(self):
        pr_test = PersonalRoom(
            name="testroom",
            owner=self.teacher_user_1,
            scheduling_strategy=self.gp.home_room_scheduling_strategy,
            is_public=False,
            config=self.test_room_config,
        )
        pr_test.save()

        pr_test.co_owners.add(self.teacher_user_1)
        pr_test.co_owners.add(self.teacher_user_2)
        pr_test.co_owners.add(self.student_user_1)
        pr_test.co_owners.add(self.student_user_2)
        pr_test.save()

        print("Owner: ", pr_test.owner)
        print("Co_Owner: ", pr_test.co_owners.all())

        self.assertEqual(len(pr_test.co_owners.all()), 3)

    def test_personal_room_is_co_owner(self):
        pr_test = PersonalRoom(
            name="testroom",
            owner=self.teacher_user_1,
            scheduling_strategy=self.gp.home_room_scheduling_strategy,
            is_public=False,
            config=self.test_room_config,
        )
        pr_test.save()

        pr_test.co_owners.add(self.teacher_user_1)
        pr_test.co_owners.add(self.teacher_user_2)
        pr_test.co_owners.add(self.student_user_1)
        pr_test.co_owners.add(self.student_user_2)
        pr_test.save()

        print("Owner: ", pr_test.owner)
        print("Co_Owner: ", pr_test.co_owners.all())

        self.assertEqual(pr_test.has_co_owner(self.teacher_user_1), False)
        self.assertEqual(pr_test.has_co_owner(self.teacher_user_2), True)
