import logging

from core.models import GeneralParameter, Meeting, Room, SchedulingStrategy, Server
from core.views import create_view_access_logging_message
from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site
from django.shortcuts import redirect, render

logger = logging.getLogger(__name__)


def statistics(request):
    general_parameter = GeneralParameter.load(get_current_site(request))
    if general_parameter.hide_statistics_enable and not request.user.is_authenticated:
        return redirect("%s?next=%s" % (settings.LOGIN_URL, request.path))
    logger.info(create_view_access_logging_message(request))
    tenant = get_current_site(request)
    return render(
        request,
        "statistics.html",
        {
            "servers": Server.objects.filter(scheduling_strategy__tenants__in=[tenant]),
            "rooms": Room.objects.filter(tenants__in=[tenant]),
            "participants_current": Room.get_participants_current(tenant),
            "performed_meetings": Meeting.objects.all().count(),
            "scheduling_strategies": SchedulingStrategy.objects.filter(tenants__in=[tenant]),
        },
    )
