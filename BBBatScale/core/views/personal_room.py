import logging

from core.forms import PersonalRoomCoOwnerForm, PersonalRoomForm
from core.models import GeneralParameter, PersonalRoom
from core.utils import set_room_config
from core.views import create_view_access_logging_message
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.sites.shortcuts import get_current_site
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


@login_required
def personal_room_create(request):
    logger.info(create_view_access_logging_message(request))
    tenant = get_current_site(request)
    form = PersonalRoomForm(request.POST or None, co_qs=request.method == "POST")
    if request.method == "POST" and form.is_valid():
        _personal_room = form.save(commit=False)
        # Custom form validations
        _personal_room.owner = request.user

        _personal_room.scheduling_strategy = GeneralParameter.load(tenant).personal_rooms_scheduling_strategy
        if _personal_room.config:
            set_room_config(_personal_room)
        _personal_room.save()
        form.save_m2m()
        _personal_room.tenants.add(request.user.tenant)
        messages.success(request, _("Personal room {} was created successfully.").format(_personal_room.name))
        # return to URL
        return redirect("home")
    return render(request, "personal_rooms_create.html", {"form": form})


@login_required
def personal_room_update(request, personal_room):
    logger.info(create_view_access_logging_message(request, personal_room))

    instance = get_object_or_404(PersonalRoom, pk=personal_room)
    if request.user == instance.owner:
        form = PersonalRoomForm(request.POST or None, instance=instance, co_qs="co_owners" in request.POST)
    else:
        form = PersonalRoomCoOwnerForm(request.POST or None, instance=instance)
    if request.method == "POST":

        if form.is_valid():
            _room = form.save(commit=False)

            if _room.config:
                set_room_config(_room)

            if request.user != instance.personalroom.owner:
                _room.name = instance.personalroom.name
                _room.owner = instance.personalroom.owner
                _room.is_public = instance.personalroom.is_public
                _room.comment_public = instance.personalroom.comment_public
            _room.save()
            form.save_m2m()
            messages.success(request, _("Personal room {} was updated successfully.").format(_room.name))
            return redirect("home")
    return render(request, "personal_rooms_create.html", {"form": form})


@login_required
def personal_room_delete(request, personal_room):
    logger.info(create_view_access_logging_message(request, personal_room))

    instance: PersonalRoom = get_object_or_404(PersonalRoom, pk=personal_room)
    if instance.owner == request.user or request.user.is_superuser:
        instance.delete()
        messages.success(request, _("Personal room was deleted successfully."))
    else:
        messages.error(request, _("Only owner can delete a personal room!"))
        logger.warning("{} has tried to delete room {} (not possible through UI)".format(request.user, instance.owner))
    return HttpResponseRedirect(request.META.get("HTTP_REFERER", "/"))
