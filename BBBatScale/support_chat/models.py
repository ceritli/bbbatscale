from __future__ import annotations

from typing import Any, Optional, Tuple, Union

from core.models import User
from django.contrib.sites.models import Site
from django.db import models, transaction
from support_chat.utils import has_supporter_privileges


def _user_to_filter_dict(column: str, user: Union[User, str]) -> dict:
    if isinstance(user, User):
        return {column: user}
    elif isinstance(user, str):
        return {column + "__username": user}
    else:
        raise ValueError("'owner' is neither of type User nor str")


def _as_user(user: Union[User, str]) -> User:
    if isinstance(user, User):
        return user
    elif isinstance(user, str):
        return User.objects.get(username=user)
    else:
        raise ValueError("'owner' is neither of type User nor str")


class SupportChatParameter(models.Model):
    tenant = models.OneToOneField(Site, on_delete=models.CASCADE)
    enabled = models.BooleanField(default=False)
    disable_chat_if_offline = models.BooleanField(default=False)
    message_max_length = models.IntegerField(default=255)

    @classmethod
    def load(cls, tenant: Site) -> SupportChatParameter:
        return cls.objects.get_or_create(tenant=tenant)[0]


class Chat(models.Model):
    id = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True, editable=False)
    support_active = models.PositiveIntegerField(default=0)
    unread_owner_messages = models.PositiveIntegerField(default=0)
    unread_support_messages = models.PositiveIntegerField(default=0)

    @classmethod
    def get(cls, owner: Union[User, str]) -> Optional[Chat]:
        return cls.objects.filter(**_user_to_filter_dict("id", owner)).first()

    @classmethod
    def get_or_create(cls, owner: Union[User, str]) -> Tuple[Chat, bool]:
        return cls.objects.get_or_create(id=_as_user(owner))

    @property
    def owner(self) -> User:
        return self.id

    @property
    def is_support_active(self) -> bool:
        return self.support_active > 0

    @property
    def has_unread_owner_messages(self) -> bool:
        return self.unread_owner_messages > 0

    @property
    def has_unread_support_messages(self) -> bool:
        return self.unread_support_messages > 0

    @property
    def has_unread_messages(self) -> bool:
        return self.has_unread_owner_messages or self.has_unread_support_messages

    def reset_statistics(self) -> Chat:
        self.support_active = 0
        self.unread_owner_messages = 0
        self.unread_support_messages = 0
        return self

    def increment_support_active(self) -> Chat:
        self.support_active += 1
        return self

    def decrement_support_active(self) -> Chat:
        self.support_active -= 1
        return self


class ChatMessage(models.Model):
    id = models.BigAutoField(primary_key=True, editable=False)
    timestamp = models.DateTimeField(auto_now_add=True, editable=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE, editable=False)
    chat = models.ForeignKey(Chat, on_delete=models.CASCADE, editable=False)
    message = models.TextField(editable=False)

    @transaction.atomic
    def save(self, *args: Any, is_supporter: Optional[bool] = None, **kwargs: Any) -> None:
        self.chat.refresh_from_db()
        if (is_supporter is None and self.chat.owner == self.user) or is_supporter is False:
            self.chat.unread_owner_messages += 1
        else:
            self.chat.unread_support_messages += 1
        self.chat.save()
        super().save(*args, **kwargs)


class PreparedAnswer(models.Model):
    tenant = models.ForeignKey(Site, on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    text = models.TextField()


class Supporter(models.Model):  # TODO add Supporter tests
    id = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True, editable=False)
    is_active = models.BooleanField(default=False)

    @classmethod
    def get(cls, user: Union[User, str]) -> Optional[Supporter]:
        return cls.objects.filter(**_user_to_filter_dict("id", user)).first()

    @classmethod
    def create(cls, user: Union[User, str]) -> Supporter:
        return cls.objects.create(id=_as_user(user))

    @classmethod
    def get_or_create(cls, user: Union[User, str]) -> Supporter:
        supporter, _ = cls.objects.get_or_create(id=_as_user(user))
        return supporter

    @classmethod
    def has_active(cls, tenant: Site) -> bool:
        return cls.objects.filter(is_active=True, id__tenant=tenant).exists()

    @property
    def user(self) -> User:
        return self.id

    def save(self, *args: Any, **kwargs: Any) -> None:
        if not has_supporter_privileges(self.user):
            raise ValueError("You do not have the privileges to be a supporter.")
        super().save(*args, **kwargs)
