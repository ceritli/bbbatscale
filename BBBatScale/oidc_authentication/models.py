from django.contrib.sites.models import SITE_CACHE, Site
from django.db import models
from django.db.models.signals import pre_delete, pre_save
from django.dispatch import receiver


class AuthParameter(models.Model):
    tenant = models.OneToOneField(Site, on_delete=models.CASCADE, verbose_name="Tenant", related_name="auth_parameter")
    client_id = models.CharField(max_length=255, verbose_name="Client ID")
    client_secret = models.CharField(max_length=255, verbose_name="Client secret")
    authorization_endpoint = models.CharField(max_length=255, verbose_name="Authorization Endpoint")
    token_endpoint = models.CharField(max_length=255, verbose_name="Token Endpoint")
    user_endpoint = models.CharField(max_length=255, verbose_name="Userinfo Endpoint")
    jwks_endpoint = models.CharField(max_length=255, verbose_name="JWKS Endpoint")
    end_session_endpoint = models.CharField(max_length=255, verbose_name="End session endpoint")
    scopes = models.CharField(max_length=255, verbose_name="Scopes", default="openid profile email")
    verify_ssl = models.BooleanField(verbose_name="Verify SSL", default=True)
    sign_algo = models.CharField(max_length=255, verbose_name="Sign algo", default="RS256")
    logout_url_method = models.CharField(
        max_length=255,
        verbose_name="Logout url method",
        default="oidc_authentication.auth.provider_logout",
        help_text="Method import path (e.g. oidc_authentication.auth.provider_logout)",
    )
    idp_sign_key = models.CharField(max_length=255, verbose_name="IDP sign key", null=True, blank=True)
    refresh_enabled = models.BooleanField(verbose_name="Refresh enabled", default=True)
    renew_id_token_expiry_seconds = models.PositiveIntegerField(
        verbose_name="Renew ID token", default=900, help_text="In seconds"
    )
    state_size = models.IntegerField(default=32, verbose_name="State size")
    use_nonce = models.BooleanField(default=True, verbose_name="Use nonce")
    nonce_size = models.IntegerField(default=32, verbose_name="Nonce size")
    allow_unsecured_jwt = models.BooleanField(default=False, verbose_name="Allow unsecured JWT")
    timeout = models.PositiveIntegerField(
        verbose_name="Timeout",
        help_text="Defines a timeout for all requests to the OIDC provider.",
        blank=True,
        null=True,
    )
    create_user = models.BooleanField(
        default=True,
        verbose_name="Create user",
        help_text="Enables or disables automatic user creation during authentication",
    )
    oidc_username_standard = models.BooleanField(
        default=True,
        verbose_name="Use oidc username standard",
        help_text='The oidc standard uses "sub" in combination with "iss" as username.',
    )
    claim_unique_username_field = models.CharField(
        max_length=255,
        default="preferred_username",
        verbose_name="Claim unique username field",
        help_text="If oidc standard for username is not used, you have to provide the claim field's name which will be "
        "combined with the domain of the corresponding tenant to create a unique username "
        "(e.g. preferred_username@domain). The provided claim field needs to be unique "
        "across the used tenant.",
    )

    def __str__(self):
        return self.__class__.__name__ + "(tenant=" + repr(self.tenant.name) + ")"


@receiver((pre_save, pre_delete), sender=AuthParameter)
def clear_site_cache_via_auth_parameter(instance, using, **_kwargs):
    """
    Clear the site cache (if primed) each time a auth parameter is saved or deleted.
    """

    try:
        del SITE_CACHE[instance.tenant_id]
    except KeyError:
        pass
    try:
        del SITE_CACHE[Site.objects.using(using).get(pk=instance.tenant_id).domain]
    except (KeyError, Site.DoesNotExist):
        pass
