{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "BBB@Scale Helm Values",
  "description": "Schema for the BBB@Scale Helm Chart values.yaml.",
  "definitions": {
    "deployment": {
      "type": "object",
      "properties": {
        "replicaCount": {
          "type": "integer"
        },
        "autoscaling": {
          "type": "object",
          "properties": {
            "enabled": {
              "type": "boolean"
            },
            "minReplicas": {
              "type": "integer",
              "minimum": 0
            },
            "maxReplicas": {
              "type": "integer",
              "minimum": 1
            },
            "targetCPUUtilizationPercentage": {
              "type": "integer",
              "minimum": 0,
              "maximum": 100
            }
          },
          "required": [
            "enabled"
          ]
        },
        "image": {
          "type": "string",
          "minLength": 1
        },
        "imageTag": {
          "type": "string"
        },
        "imagePullPolicy": {
          "type": "string",
          "enum": [
            "Always",
            "Never",
            "IfNotPresent"
          ]
        },
        "imagePullSecrets": {
          "type": "array",
          "items": {
            "type": "string"
          },
          "minItems": 0
        },
        "annotations": {
          "type": "object"
        },
        "podAnnotations": {
          "type": "object"
        },
        "resources": {
          "type": "object"
        }
      }
    },
    "secret": {
      "oneOf": [
        {
          "type": "string",
          "minLength": 1
        },
        {
          "type": "object",
          "properties": {
            "secretName": {
              "type": "string",
              "minLength": 1
            },
            "secretKey": {
              "type": "string",
              "minLength": 1
            }
          },
          "required": [
            "secretName",
            "secretKey"
          ]
        }
      ]
    }
  },
  "type": "object",
  "properties": {
    "bbbatscale": {
      "type": "object",
      "allOf": [
        {
          "$ref": "#/definitions/deployment"
        },
        {
          "properties": {
            "domains": {
              "type": "array",
              "items": {
                "type": "string"
              },
              "minItems": 1
            },
            "djangoSettingsModule": {
              "type": "string"
            },
            "djangoSecretKey": {
              "type": "string",
              "minLength": 1
            },
            "recordingsSecret": {
              "type": "string",
              "minLength": 1
            },
            "moderatorsGroupName": {
              "type": "string",
              "minLength": 1
            },
            "supportersGroupName": {
              "type": "string",
              "minLength": 1
            },
            "logging": {
              "type": "object",
              "properties": {
                "enabled": {
                  "type": "boolean"
                },
                "config": {
                  "type": "string"
                }
              }
            },
            "supportChat": {
              "type": "object",
              "properties": {
                "enabled": {
                  "type": "boolean"
                }
              }
            },
            "oidc": {
              "type": "object",
              "properties": {
                "enabled": {
                  "type": "boolean"
                },
                "refreshEnabled": {
                  "type": "boolean"
                }
              }
            },
            "webhooks": {
              "type": "object",
              "properties": {
                "enabled": {
                  "type": "boolean"
                },
                "worker": {
                  "$ref": "#/definitions/deployment"
                }
              }
            }
          }
        }
      ],
      "required": [
        "domains",
        "djangoSecretKey",
        "recordingsSecret"
      ]
    },
    "postgresql": {
      "description": "Postgres configurations (for more information see https://github.com/bitnami/charts/blob/master/bitnami/postgresql/README.md).",
      "type": "object",
      "properties": {
        "enabled": {
          "description": "Enables the single postgres deployment. Another postgres deployment may not be enabled at the same time.",
          "type": "boolean"
        }
      }
    },
    "postgresql-ha": {
      "description": "Postgres-HA configurations (for more information see https://github.com/bitnami/charts/blob/master/bitnami/postgresql-ha/README.md).",
      "type": "object",
      "properties": {
        "enabled": {
          "description": "Enables the postgres-ha deployment. Another postgres deployment may not be enabled at the same time.",
          "type": "boolean"
        }
      }
    },
    "postgresql-extern": {
      "description": "External Postgres configurations.",
      "type": "object",
      "properties": {
        "enabled": {
          "description": "Connects the application to an external postgres deployment. Another postgres deployment may not be enabled at the same time.",
          "type": "boolean"
        },
        "database": {
        },
        "username": {
        },
        "password": {
        },
        "host": {
        },
        "port": {
        },
        "manifests": {
          "description": "Additional kubernetes manifests to apply to the cluster when using an external postgres service."
        }
      }
    },
    "redis": {
      "description": "Redis configurations (for more information see https://github.com/bitnami/charts/blob/master/bitnami/redis/README.md).",
      "type": "object"
    },
    "nginx": {
      "type": "object",
      "allOf": [
        {
          "$ref": "#/definitions/deployment"
        },
        {
          "properties": {
            "xForwardedProto": {
              "type": "string"
            },
            "port": {
              "type": "integer",
              "minimum": 1,
              "maximum": 65535
            },
            "serviceType": {
              "type": "string",
              "enum": [
                "ClusterIP",
                "NodePort",
                "LoadBalancer",
                "ExternalName"
              ]
            },
            "additionalServerConfig": {
              "type": "string"
            }
          }
        }
      ]
    },
    "additionalManifests": {
      "description": "Additional kubernetes manifests to apply to the cluster.",
      "type": "array",
      "items": {
        "type": "object"
      }
    },
    "ingress": {
      "type": "object",
      "properties": {
        "enabled": {
          "type": "boolean"
        },
        "annotations": {
          "type": "object"
        },
        "tls": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "secretName": {
                "type": "string"
              },
              "hosts": {
                "type": "array",
                "items": {
                  "type": "string"
                },
                "minItems": 1
              }
            }
          }
        }
      }
    }
  },
  "allOf": [
    {
      "oneOf": [
        {
          "properties": {
            "postgresql": {
              "type": "object",
              "properties": {
                "enabled": {
                  "const": true
                }
              }
            },
            "postgresql-ha": {
              "type": "object",
              "properties": {
                "enabled": {
                  "const": false
                }
              }
            },
            "postgresql-extern": {
              "type": "object",
              "properties": {
                "enabled": {
                  "const": false
                }
              }
            }
          }
        },
        {
          "properties": {
            "postgresql": {
              "type": "object",
              "properties": {
                "enabled": {
                  "const": false
                }
              }
            },
            "postgresql-ha": {
              "type": "object",
              "properties": {
                "enabled": {
                  "const": true
                }
              }
            },
            "postgresql-extern": {
              "type": "object",
              "properties": {
                "enabled": {
                  "const": false
                }
              }
            }
          }
        },
        {
          "properties": {
            "postgresql": {
              "type": "object",
              "properties": {
                "enabled": {
                  "const": false
                }
              }
            },
            "postgresql-ha": {
              "type": "object",
              "properties": {
                "enabled": {
                  "const": false
                }
              }
            },
            "postgresql-extern": {
              "type": "object",
              "properties": {
                "enabled": {
                  "const": true
                }
              }
            }
          }
        }
      ]
    },
    {
      "if": {
        "properties": {
          "postgresql-extern": {
            "type": "object",
            "properties": {
              "enabled": {
                "const": true
              }
            }
          }
        }
      },
      "then": {
        "properties": {
          "postgresql-extern": {
            "type": "object",
            "properties": {
              "enabled": {
                "const": true
              },
              "database": {
                "$ref": "#/definitions/secret"
              },
              "username": {
                "$ref": "#/definitions/secret"
              },
              "password": {
                "$ref": "#/definitions/secret"
              },
              "host": {
                "$ref": "#/definitions/secret"
              },
              "port": {
                "oneOf": [
                  {
                    "$ref": "#/definitions/secret"
                  },
                  {
                    "type": "integer",
                    "minimum": 1,
                    "maximum": 65535
                  }
                ]
              },
              "manifests": {
                "type": "array",
                "items": {
                  "type": "object"
                }
              }
            },
            "required": [
              "database",
              "username",
              "password",
              "host"
            ]
          }
        }
      }
    }
  ],
  "required": [
    "bbbatscale"
  ]
}
